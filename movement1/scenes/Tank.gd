extends Node2D

# Assigned by Main.
var target

var linear_velocity
var rotational_velocity

const max_acceleration = 100
const max_speed = 50

var steering

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	position.x = rand_range(0, 500)
	position.y = rand_range(250, 500)
	linear_velocity = Vector2(0, 0)
	rotational_velocity = 0
	
	var Steering = preload("res://scenes/Steering.tscn")
	steering = Steering.instance()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Compute steering.
	steering.linear = target.position -  position
	steering.linear = steering.linear.normalized()
	steering.linear *= max_acceleration
	
	steering.angular = 0.0
	
	# Update tank.
	movement_update(delta)

func movement_update(delta):
	var old_position = position
	position += linear_velocity * delta
	rotation += rotational_velocity * delta
	
	linear_velocity += steering.linear * delta
	rotational_velocity += steering.angular * delta
	
	if linear_velocity.length() > max_speed:
		linear_velocity = linear_velocity.normalized() * max_speed
	print(steering.linear)
	
	var direction = position - old_position
	rotation = atan2(direction.y, direction.x)