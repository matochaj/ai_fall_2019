extends Node

export var Piece : PackedScene
var PIECE_SIZE = 200
var blank_row
var blank_col

# Called when the node enters the scene tree for the first time.
func _ready():
	var original_board = [	[1, 2, 3],
							[8, 0, 4],
							[7, 6, 5]]
	for row in original_board.size():
		for col in original_board[row].size():
			var new_piece = Piece.instance()
			new_piece.position = Vector2(col*PIECE_SIZE, row*PIECE_SIZE)
			new_piece.get_node("Label").text = str(original_board[row][col])
			if original_board[row][col] == 0:
				new_piece.visible = false
				blank_row = row
				blank_col = col
			add_child(new_piece)
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _input(event):
	if event.is_action_pressed("click"):
		var row = int(event.get_position().y) / PIECE_SIZE
		var col = int(event.get_position().x) / PIECE_SIZE
		
		if abs(row-blank_row) == 1 && abs(col-blank_col) == 0 || \
			abs(row-blank_row) == 0 && abs(col-blank_col) == 1:
			print("movable")