extends Node2D

var Player = preload("res://Scenes/Player2.tscn")
var current_player

# Called when the node enters the scene tree for the first time.
func _ready():
	current_player = Player.instance()
	add_child(current_player)
	current_player.increase_score()