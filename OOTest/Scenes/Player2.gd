extends "res://Player.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	increase_score()

func increase_score():
	score += 2
	print("Player 2: " + str(score))