extends "res://Player.gd"

# Called when the node enters the scene tree for the first time.
func _ready():
	increase_score()

func increase_score():
	score += 1
	print("Player 1: " + str(score))