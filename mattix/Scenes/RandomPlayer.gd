extends "res://Scenes/Player.gd"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Pick a random number from -10 to 7 or has a 50% chance of returning
# the string "Bogomove".  Do not worry if it is valid.
func make_move(board, is_col_move, cursor_row, cursor_col):
	var move = int(rand_range(-10, 8))
	if rand_range(0,1) < .5:
		move = "Bogomove"
	print("Making move: " + str(move))
	emit_signal("move_complete", move)