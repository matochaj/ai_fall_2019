extends Node

var Player1 = preload("res://Scenes/HumanPlayer.tscn")
var Player2 = preload("res://Scenes/RandomPlayer.tscn")

export var Piece: PackedScene
var board_y_offset = 50
var current_board # A 2d array of Piece refs representing the current state of the board.
var cursor_row
var cursor_col
var is_col_move
var players = []
var current_player

var x_size: int
var y_size: int

# Called when the node enters the scene tree for the first time.
func _ready():
	build_board(4)

	players.append(Player1.instance())
	players.append(Player2.instance())
	for player in players:
		add_child(player)
		# Connect the signal from the Players to be handled here.
		player.connect("move_complete", self, "handle_completed_move")
		
	current_player = 0 # Whose turn it is.
	is_col_move = true
	
	highlight_pieces(current_board, is_col_move, cursor_row, cursor_col)
	# Ask the current player to make a move.
	print("Player ", current_player)
	players[current_player].make_move(current_board, is_col_move, cursor_row, cursor_col)

# This function is fired by the Player's signal.  It picks up the move from the signal.
func handle_completed_move(move):	
	if is_valid_move(move):
		print("move handled: " + str(move))
		var points = make_move(move)
		players[current_player].add_points(points)
		
		prepare_for_next_move()
		
		if !is_game_over(current_board, is_col_move, cursor_row, cursor_col):
			players[current_player].make_move(current_board, is_col_move, cursor_row, cursor_col)
#		else:
#			print("Player 1: ", players[0].get_score(), ", Player 2: ", players[1].get_score())

	else: # The move was invalid.
#		print("move ignored: " + str(move))
		players[current_player].make_move(current_board, is_col_move, cursor_row, cursor_col)

func prepare_for_next_move():
	# Kick off next player's move.
	is_col_move = !is_col_move
	current_player = (current_player+1) % 2
	print("Player: ", current_player)
	print("Player 0: ", players[0].get_score(), ", Player 1: ", players[1].get_score())
	print_board()
	print("-------------------------------------------------------")
	highlight_pieces(current_board, is_col_move, cursor_row, cursor_col)
	$Player0_score.text = "Player 1:\n"+str(players[0].get_score())
	$Player1_score.text = "Player 2:\n"+str(players[1].get_score())

func make_move(move):
	print("Move: ", move)
	print("Cursor: ", cursor_row, ", ", cursor_col)
	# Remove the current piece from the board (make it null).
	var move_points
	if is_col_move:
		move_points = int(current_board[move][cursor_col].get_node("Label").text)
		remove_child(current_board[move][cursor_col])
		# Move cursor to the empty position.
		current_board[move][cursor_col] = current_board[cursor_row][cursor_col]
		current_board[cursor_row][cursor_col] = null
		cursor_row = move
	else:
		move_points = int(current_board[cursor_row][move].get_node("Label").text)
		remove_child(current_board[cursor_row][move])
		# Move cursor to the empty position.
		current_board[cursor_row][move] = current_board[cursor_row][cursor_col]
		current_board[cursor_row][cursor_col] = null
		cursor_col = move

	print(cursor_row, cursor_col)
	current_board[cursor_row][cursor_col].position.x = cursor_col*x_size
	current_board[cursor_row][cursor_col].position.y = board_y_offset + cursor_row*y_size
	
	return move_points

func is_valid_move(move):
	if typeof(move) == TYPE_INT:
		if is_col_move:
			return move != cursor_row && move >= 0 && move < current_board.size() && current_board[move][cursor_col]
		else:
			return move != cursor_col && move >= 0 && move < current_board[cursor_row].size() && current_board[cursor_row][move]
	else:
		return false # 'move' was not an integer

func highlight_pieces(board, is_col_move, cursor_row, cursor_col):
	# First, unhighlight all pieces.
	for row in current_board:
		for piece in row:
			if piece != null:
				piece.unhighlight()
				
	if is_col_move:
		for row in board:
			if row[cursor_col] != null:
				row[cursor_col].highlight()
		pass
	else:
		for piece in board[cursor_row]:
			if piece != null:
				piece.highlight()

func is_game_over(board, is_col_move, cursor_row, cursor_col):
	# Check current row or column to see if there are any pieces.
	if is_col_move:
		for row in board:
			if row[cursor_col] != null && row[cursor_col].get_node("Label").text != "*": # We found a piece.
				return false
	else:
		for piece in board[cursor_row]:
			if piece != null && piece.get_node("Label").text != "*":
				return false
	print("GAME OVER!")
	return true

func build_board(board_size):
	var screen_width = 500 # Should probably detect the screen size.
	var original_pieces =	[[10, 8, 7, 6, 5, 4, 4, 4, 3, 3, 3, 2, 2, 1, 0, "*"], # 4x4 board
							[15, 10, 9, 8, 7, 7, 6, 6, 5, 5, 4, 4, 4, 3, 3, 3, 2, 2, 2, 1, 1, 0, 0, 0, "*"],  # 5x5 board
							[15, 9, 8, 7, 6, 6, 5, 5, 4, 4, 3, 3, 3, 2, 2, 2, 2, 2, 1, 1, 1, 0, 0, 0, -1, -1, -1, -2, -2, -3, -3, -4, -4, -5, -6, "*"], # 6x6 board
							[15, 9, 8, 8, 7, 7, 6, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 3, 2, 2, 2, 2, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, -1, -1, -1, -1, -2, -2, -2, -3, -3, -3, -4, -4, -5, -5, -6, -7, "*"], # 7x7 board
							[15, 10, 9, 9, 8, 8, 7, 7, 7, 6, 6, 6, 5, 5, 5, 5, 4, 4, 4, 4, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, -1, -1, -1, -1, -1, -2, -2, -2, -2, -3, -3, -3, -4, -4, -4, -5, -5, -6, -6, -7, -10, "*"]] # 8x8 board
	var pieces_index = board_size-4 # Index into original_pieces choosing game size (0=4x4, 1=5x5, 2=6x6, 3=7x7, 4=8x8).
	var current_pieces = original_pieces[pieces_index].duplicate() # Copy since destructively altered below.
	var tiles_per_side = int(sqrt(current_pieces.size()))

	x_size = screen_width / tiles_per_side 
	y_size = screen_width / tiles_per_side

	randomize() # Comment for repeatable piece order.

	current_board = []
	for piece_index in current_pieces.size():
		var new_piece = Piece.instance()

		var col = piece_index / tiles_per_side
		var row = piece_index % tiles_per_side
		if col == 0:
			current_board.append([])

		var random_index = rand_range(0, current_pieces.size())
		if str(current_pieces[random_index]) == "*":
			cursor_row = row
			cursor_col = col
		new_piece.setup(col*x_size, board_y_offset + row*y_size, x_size, y_size, current_pieces[random_index])
		current_pieces.remove(random_index) # Destructive
		current_board[row].append(new_piece) # Place a ref to the current piece into the current_board.

		add_child(new_piece)
	
	print_board()

func print_board():
	for row in current_board:
		var row_string = ""
		for piece in row:
			if piece != null:
				row_string += piece.get_node("Label").text +"\t"
			else:
				row_string += "-" +"\t"
		print(row_string+"\n")