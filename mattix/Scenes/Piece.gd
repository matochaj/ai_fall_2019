extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func setup(x, y, w, h, text):
	position.x = x
	position.y = y
	
	scale.x = w / $Sprite.get_texture().get_size().x
	scale.y = h / $Sprite.get_texture().get_size().y
	
	$Label.text = str(text)
	
	# Tint the "cursor".
	if $Label.text == "*":
		$Sprite.modulate = Color(1, .5, .5)

# Mark this piece as selectable.
func highlight():
	if $Label.text != '*':
		$Sprite.modulate = Color(.8, 1, .8)

func unhighlight():
	if $Label.text != '*':
		$Sprite.modulate = Color(1, 1, 1)