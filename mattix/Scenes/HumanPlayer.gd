extends "res://Scenes/Player.gd"

var listening
var board
var is_col_move
var cursor_row
var cursor_col

# Less cheaty, but
# Can we get board_y_offset from main?  Do we care?
var board_y_offset = 50
var x_size
var y_size

# Called when the node enters the scene tree for the first time.
func _ready():
	listening = false
	

func make_move(b, col_move, c_row, c_col):
	board = b
	is_col_move = col_move
	cursor_row = c_row
	cursor_col = c_col
	
	var screen_size = get_viewport_rect().size
	x_size = screen_size.x/b[0].size()
	y_size = screen_size.x/b.size()
	
	listening = true
	print("Waiting on a click")

func _input(event):
	if listening && event.is_action_pressed("click"):
		var row = int((event.position.y - board_y_offset) / y_size)
		var col = int(event.position.x/x_size)
		print("Clicked ", row, ", ", col)
		
		var move
		# If in the correct row/col, stop listening and "return" move.
		if is_col_move && col == cursor_col:
			listening = false
			move = row
			emit_signal("move_complete", move)
		elif !is_col_move && row == cursor_row:
			listening = false
			move = col
			emit_signal("move_complete", move)
		# Otherwise, wait for more input.