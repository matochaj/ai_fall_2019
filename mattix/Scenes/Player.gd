extends Node2D

signal move_complete

var score

# Called when the node enters the scene tree for the first time.
func _ready():
	score = 0

# "Abstract" method to be defined in child classes.
func make_move(board, is_col_move, cursor_row, cursor_col):
	assert(false)

func add_points(points):
	score += points

func get_score():
	return score